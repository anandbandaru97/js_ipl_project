// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePathDeliveries = 'src/data/deliveries.csv';
const csvFilePathMatches = 'src/data/matches.csv';

fs.readFile(csvFilePathMatches, 'utf-8', (err, data) => {
    if (err) {
        console.error('Error reading CSV file:', err);
        return;
    }

    // Parse the CSV data 
    const parsedDataMatches = Papa.parse(data, {
        header: true, // Treat the first row as header
    });

    fs.readFile(csvFilePathDeliveries, 'utf-8', (err, data1) => {
        if (err) {
            console.error('Error reading CSV file:', err);
            return;
        }

        // Parse the CSV data 
        const parsedDataDeliveries = Papa.parse(data1, {
            header: true, // Treat the first row as header
        });

        let matchesData = parsedDataMatches.data;
        let deliveriesData = parsedDataDeliveries.data;

        const getTopTenEconomicalBowlersIn2015 = (matchesData, deliveriesData) => {

            // To get Match Id
            let matchId = new Set();
            for (let match of matchesData) {
                if (match['season'] === '2015' && match['season'] != undefined) {
                    matchId.add(match['id']);
                }
            }

            // To get Bowler, Runs, Balls
            let bowlersEconomy = {};
            for (let delivery of deliveriesData) {
                if (matchId.has(delivery['match_id']) || delivery['bowler'] == undefined) {
                    bowlersEconomy[delivery['bowler']] = [parseInt(delivery['total_runs']), 1];
                } else if (bowlersEconomy[delivery['bowler']] !== undefined) {
                    bowlersEconomy[delivery['bowler']] = [bowlersEconomy[delivery['bowler']][0] + parseInt(delivery['total_runs']), bowlersEconomy[delivery['bowler']][1] + 1];
                }
            }

            // To get Bowler Economy
            for (let economy in bowlersEconomy) {
                bowlersEconomy[economy] = parseFloat((bowlersEconomy[economy][0] / bowlersEconomy[economy][1]).toFixed(2));
            }

            // To get top 10 Bowlers using sort
            let topTenBowlers = Object.entries(bowlersEconomy).sort((bowler1, bowler2) => bowler1[1] - bowler2[1]);
            return topTenBowlers.slice(-10);
        }

        let result = getTopTenEconomicalBowlersIn2015(matchesData, deliveriesData);
        let jsonData = JSON.stringify(result);

        fs.writeFile('src/public/output/4-top-10-economical-bowlers.json', jsonData, (err) => {
            if (err) {
                console.error('Error writing JSON data to file:', err);
             }
             console.log('CSV data converted to JSON and saved as output.json');
        })
    })
})