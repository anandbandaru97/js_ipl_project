// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePathDeliveries = 'src/data/deliveries.csv';
const csvFilePathMatches = 'src/data/matches.csv';

fs.readFile(csvFilePathMatches, 'utf-8', (err, data) => {
    if (err) {
        console.error('Error reading CSV file:', err);
        return;
    }

    // Parse the CSV data 
    const parsedDataMatches = Papa.parse(data, {
        header: true, // Treat the first row as header
    });

    fs.readFile(csvFilePathDeliveries, 'utf-8', (err, data1) => {
        if (err) {
            console.error('Error reading CSV file:', err);
            return;
        }

        // Parse the CSV data 
        const parsedDataDeliveries = Papa.parse(data1, {
            header: true, // Treat the first row as header
        });

        let matchesData = parsedDataMatches.data;
        let deliveriesData = parsedDataDeliveries.data;

        function strikerateOfBatsmenEverySeason(matchesData, deliveriesData) {
            const distinctYears = [];
            for (let season in matchesData) {
                const year = matchesData[season]['season'];
                if (year !== undefined && distinctYears.indexOf(year) === -1) {
                    distinctYears.push(year);
                }
            }
            //console.log(distinctYears);

            const matchIdForSeason = {};
            for (let season in distinctYears) {
                const year = distinctYears[season];
                const yearList = [];
                for (let match of matchesData) {
                    let matchId = match['id'];
                    let matchSeason = match['season'];
                    if (matchSeason === year) {
                        yearList.push(matchId);
                    }
                }
                matchIdForSeason[year] = yearList;
            }
            //console.log(matchIdForSeason);

            const toatlRunsByBatsmanSeason = {};
            for (let season in distinctYears) {
                const year = distinctYears[season];
                const result = {};
                for (let delivery of deliveriesData) {
                    if (matchIdForSeason[year].includes(delivery['match_id'])) {
                        const batsman = delivery['batsman'];
                        const runs = parseInt(delivery['batsman_runs']);
                        const wideRuns = delivery['wide_runs'];
                        const noballRuns = delivery['noball_runs'];

                        if (!result[batsman]) {
                            result[batsman] = { runs: 0, balls: 0 };
                        }

                        result[batsman].runs += runs;

                        if (wideRuns === '0' && noballRuns === '0') {
                            result[batsman].balls++;
                        }
                    }
                }
                toatlRunsByBatsmanSeason[year] = result;
            }

            const strikeRateByBatsmanSeason = {};
            for (let season in distinctYears) {
                const year = distinctYears[season];
                const yearStats = {};
                const batsmenStats = Object.entries(toatlRunsByBatsmanSeason[year]);
                for (let batsmen in batsmenStats) {
                    const [batsman, stats] = batsmenStats[batsmen];
                    const { runs, balls } = stats;
                    yearStats[batsman] = parseFloat(((runs / balls) * 100).toFixed(2));
                }
                strikeRateByBatsmanSeason[year] = yearStats;
            }

            return strikeRateByBatsmanSeason;
        }

        let output = strikerateOfBatsmenEverySeason(matchesData, deliveriesData);
        const jsonData = JSON.stringify(output);

        fs.writeFile('src/public/output/8-highest-no-of-times-dismissed-by-player.json', jsonData, 'utf-8', (err) => {
            if (err) {
                console.error('Error writing JSON data to file:', err);
                return;
            }
            console.log('CSV data converted to JSON and saved as output.json');
        });
    });
});