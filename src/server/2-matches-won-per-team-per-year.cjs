// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePath = 'src/data/matches.csv';
fs.readFile(csvFilePath, 'utf-8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }

    // Parse the CSV data
    const parsedData = Papa.parse(data, {
        header: true, //Treat the first row as header
    });

    const matchesOfAllYears = parsedData.data;

    const matchesWonPerTeamPerYear = (matchesOfAllYears) => {
        const matchesWon = matchesOfAllYears.reduce((accumulator, match) => {
            let winner = match['winner'];
            let season = match['season'];
            if (accumulator[winner] === undefined && winner !== null) {
                accumulator[winner] = {};
                accumulator[winner][season] = 1;
                return accumulator;
            } else if(accumulator[winner][season] === undefined) {
                accumulator[winner][season] = 1;
            }
            accumulator[winner][season] += 1;
            return accumulator;
        }, {})
        return matchesWon;
    }

    //console.log(matchesWonPerTeamPerYear(matchesOfAllYears));

    let result = matchesWonPerTeamPerYear(matchesOfAllYears);
    const jsonData = JSON.stringify(result);

    fs.writeFile('src/public/output/2-matches-won-per-team-per-year.json', jsonData, 'utf-8', (err) => {
        if (err) {
            console.error('Error writing JSON data to file:', err);
            return;
        }
        console.log('CSV data converted to JSON and saved as output.json');
    })
});