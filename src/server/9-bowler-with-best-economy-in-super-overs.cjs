// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePathDeliveries = 'src/data/deliveries.csv';

fs.readFile(csvFilePathDeliveries, 'utf-8', (err, data1) => {
    if (err) {
        console.error('Error reading CSV file:', err);
        return;
    }

    // Parse the CSV data 
    const parsedDataDeliveries = Papa.parse(data1, {
        header: true, // Treat the first row as header
    });

    let deliveriesData = parsedDataDeliveries.data;

    function superOverBowlerBestEconomy(deliveriesData) {
        let bowlersEconomy = {};
        for (let delivery of deliveriesData) {
            let superOver = delivery['is_super_over'];
            let bowler = delivery['bowler'];
            let totalRuns = parseInt(delivery['total_runs']);
            if (superOver === '1') {
                if (!bowlersEconomy[bowler]) {
                    bowlersEconomy[bowler] = { 'totalRuns': 0, 'balls': 0 };
                }

                if (!bowlersEconomy[bowler]['totalRuns']) {
                    bowlersEconomy[bowler]['totalRuns'] = totalRuns;
                    bowlersEconomy[bowler]['balls'] = 1;
                } else {
                    bowlersEconomy[bowler]['totalRuns'] += totalRuns;
                    bowlersEconomy[bowler]['balls'] += 1;
                }
            }
        }

        let allBowlersEconomy = Object.entries(bowlersEconomy);
        let bowlerEconomy = {};
        for (let economy of allBowlersEconomy) {
            let bowler = economy[0];
            let totalRuns = economy[1]["totalRuns"];
            let overs = economy[1]['balls'] / 6;
            bowlerEconomy[bowler] = (totalRuns / overs).toFixed(2);
        }
        let bestEconomy = Object.entries(bowlerEconomy).sort((bowler1, bowler2) => bowler1[1] - bowler2[1]);
        return bestEconomy[0];
    }

    const result = superOverBowlerBestEconomy(deliveriesData);
    const jsonData = JSON.stringify(result);

    fs.writeFile('src/public/output/9-bowler-with-best-economy-in-super-overs.json', jsonData, 'utf-8', (err) => {
        if (err) {
            console.error('Error writing JSON data to the file:', err)
        }
        console.log('CSV data converted to JSON and saved as output.json');
    })
});
