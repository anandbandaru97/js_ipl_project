// Require the necessary modules
const fs = require('fs');
const Papa = require('papaparse');

// Read the CSV file
const csvFilePath = 'src/data/matches.csv';
fs.readFile(csvFilePath, 'utf-8', (err, data) => {
    if (err) {
        console.error('Error reading the CSV file:', err);
        return;
    }

    // Parse the CSV data
    const parsedData = Papa.parse(data, {
        header: true, //Treat the first row as header
    });

    const matchesData = parsedData.data;

    const getPlayerOfTheMatchesEachSeason = (matchesData) => {
        let playerOfTheMatch = {}
        for (let match of matchesData) {
            if (playerOfTheMatch[match['season']] === undefined) {
                playerOfTheMatch[match['season']] = {};
            } else if (playerOfTheMatch[match['season']][match['player_of_match']] === undefined) {
                playerOfTheMatch[match['season']][match['player_of_match']] = 1;
            } else if (playerOfTheMatch[match['season']][match['player_of_match']] !== undefined) {
                playerOfTheMatch[match['season']][match['player_of_match']] += 1;
            }
        }

        let playerOfTheMatchEachSeason = {};
        for (let [year, player] of Object.entries(playerOfTheMatch)) {
            let playerOfMatch = Object.entries(player).sort((player1, player2) => player1[1] - player2[1]);
            playerOfTheMatchEachSeason[year] = playerOfMatch.slice(-1)[0][0];
        }
        return playerOfTheMatchEachSeason;
    }
    //console.log(getPlayerOfTheMatchesEachSeason(matchesData));

    let result = getPlayerOfTheMatchesEachSeason(matchesData);
    const jsonData = JSON.stringify(result);

    fs.writeFile('src/public/output/6-highest-player-of-match-awards-per-season.json', jsonData, 'utf-8', (err) => {
        if (err) {
            console.error('Error writing JSON data to file:', err);
            return;
        }
        console.log('CSV data converted to JSON and saved as output.json');
    });
});